DEVICE := m2081

SDAT_SUFFIXES = .new.dat.br .patch.dat .transfer.list

MODEM_IMG := firmware-update/NON-HLOS.bin
VBMETA_MBN := firmware-update/vbmeta.mbn
VBMETA_SYSTEM_IMG := firmware-update/vbmeta_system.img
VENDOR_SDAT := $(addprefix vendor,$(SDAT_SUFFIXES))

TIMESTAMP := $(shell strings $(MODEM_IMG) | grep -m 1 '"Time_Stamp"' | sed -n 's|.*"Time_Stamp": "\([^"]*\)"|\1|p')
VERSION := $(shell echo $(TIMESTAMP) | sed 's|[ :-]*||g')

HASH_VBMETA := $(shell openssl dgst -r -sha1 $(VBMETA_MBN) | cut -d ' ' -f 1)
HASH_VBMETA_SYSTEM := $(shell openssl dgst -r -sha1 $(VBMETA_SYSTEM_IMG) | cut -d ' ' -f 1)

TARGET := RADIO-$(DEVICE)-$(VERSION).zip

# Build
# ==========

.PHONY: build
build: assert inspect $(TARGET)
	@echo Size: $(shell stat -f %z $(TARGET))

LIST := META-INF
LIST += firmware-update
LIST += dynamic_partitions_op_list
LIST += $(VENDOR_SDAT)
$(TARGET): $(LIST)
	zip -r0 $@ $(filter %.new.dat.br,$^)
	zip -r9 $@ $(filter-out %.new.dat.br,$^)

# Clean
# ==========

.PHONY: clean
clean:
	rm -f *.zip

# Assert
# ==========
.PHONY: assert
assert: $(VBMETA_MBN) $(VBMETA_SYSTEM_IMG)
ifneq ($(HASH_VBMETA), b46e10a80f66636332e2c51956f9e818c8189a62)
	$(error SHA-1 of $(notdir $(VBMETA_MBN)) mismatch)
endif
ifneq ($(HASH_VBMETA_SYSTEM), d7b0ecb0a9f9d53dbd4cf21f104c3a29e4608e27)
	$(error SHA-1 of $(notdir $(VBMETA_SYSTEM_IMG)) mismatch)
endif
	@echo Everything is ok.

# Inspect
# ==========

.PHONY: inspect
inspect: $(MODEM_IMG)
	@echo Target: $(TARGET)
	@echo Timestamp: $(TIMESTAMP)
